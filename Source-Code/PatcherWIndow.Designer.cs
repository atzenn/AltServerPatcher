﻿namespace AltServerPatcher
{
    partial class PatcherWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PatcherWindow));
            this.UtilityLabel = new System.Windows.Forms.Label();
            this.IPASelector = new System.Windows.Forms.ComboBox();
            this.PatchButton = new System.Windows.Forms.Button();
            this.IPAURLLabel = new System.Windows.Forms.Label();
            this.IPAURLBox = new System.Windows.Forms.TextBox();
            this.RestoreOriginalButton = new System.Windows.Forms.LinkLabel();
            this.Invoxi = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.about = new System.Windows.Forms.Label();
            this.StatusProgress = new System.Windows.Forms.ProgressBar();
            this.version = new System.Windows.Forms.Label();
            this.progressBarEx1 = new QuantumConcepts.Common.Forms.UI.Controls.ProgressBarEx();
            this.SuspendLayout();
            // 
            // UtilityLabel
            // 
            this.UtilityLabel.AutoSize = true;
            this.UtilityLabel.Location = new System.Drawing.Point(30, 16);
            this.UtilityLabel.Name = "UtilityLabel";
            this.UtilityLabel.Size = new System.Drawing.Size(62, 13);
            this.UtilityLabel.TabIndex = 0;
            this.UtilityLabel.Text = "Select App:";
            // 
            // IPASelector
            // 
            this.IPASelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IPASelector.FormattingEnabled = true;
            this.IPASelector.Items.AddRange(new object[] {
            "unc0ver",
            "Chimera",
            "AltStore",
            "Custom URL"});
            this.IPASelector.Location = new System.Drawing.Point(95, 12);
            this.IPASelector.Name = "IPASelector";
            this.IPASelector.Size = new System.Drawing.Size(261, 21);
            this.IPASelector.TabIndex = 1;
            this.IPASelector.SelectedIndexChanged += new System.EventHandler(this.IPASelector_SelectedIndexChanged);
            // 
            // PatchButton
            // 
            this.PatchButton.Location = new System.Drawing.Point(362, 11);
            this.PatchButton.Name = "PatchButton";
            this.PatchButton.Size = new System.Drawing.Size(75, 23);
            this.PatchButton.TabIndex = 2;
            this.PatchButton.Text = "Patch";
            this.PatchButton.UseVisualStyleBackColor = true;
            this.PatchButton.Click += new System.EventHandler(this.PatchButton_Click);
            // 
            // IPAURLLabel
            // 
            this.IPAURLLabel.AutoSize = true;
            this.IPAURLLabel.Location = new System.Drawing.Point(30, 43);
            this.IPAURLLabel.Name = "IPAURLLabel";
            this.IPAURLLabel.Size = new System.Drawing.Size(60, 13);
            this.IPAURLLabel.TabIndex = 3;
            this.IPAURLLabel.Text = "Enter URL:";
            // 
            // IPAURLBox
            // 
            this.IPAURLBox.Location = new System.Drawing.Point(113, 40);
            this.IPAURLBox.MaxLength = 2048;
            this.IPAURLBox.Name = "IPAURLBox";
            this.IPAURLBox.ReadOnly = true;
            this.IPAURLBox.Size = new System.Drawing.Size(324, 20);
            this.IPAURLBox.TabIndex = 4;
            // 
            // RestoreOriginalButton
            // 
            this.RestoreOriginalButton.AutoSize = true;
            this.RestoreOriginalButton.Location = new System.Drawing.Point(12, 129);
            this.RestoreOriginalButton.Name = "RestoreOriginalButton";
            this.RestoreOriginalButton.Size = new System.Drawing.Size(44, 13);
            this.RestoreOriginalButton.TabIndex = 5;
            this.RestoreOriginalButton.TabStop = true;
            this.RestoreOriginalButton.Text = "Restore";
            this.RestoreOriginalButton.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.RestoreOriginalButton_LinkClicked);
            // 
            // Invoxi
            // 
            this.Invoxi.AutoSize = true;
            this.Invoxi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Invoxi.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.Invoxi.ForeColor = System.Drawing.Color.DarkRed;
            this.Invoxi.Location = new System.Drawing.Point(301, 130);
            this.Invoxi.Name = "Invoxi";
            this.Invoxi.Size = new System.Drawing.Size(149, 14);
            this.Invoxi.TabIndex = 6;
            this.Invoxi.Text = "Made by InvoxiPlayGames";
            this.Invoxi.Click += new System.EventHandler(this.Invoxi_Click);
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.BackColor = System.Drawing.Color.Transparent;
            this.StatusLabel.Location = new System.Drawing.Point(12, 65);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(0, 13);
            this.StatusLabel.TabIndex = 7;
            // 
            // about
            // 
            this.about.AutoSize = true;
            this.about.Cursor = System.Windows.Forms.Cursors.Hand;
            this.about.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.about.Location = new System.Drawing.Point(204, 97);
            this.about.Name = "about";
            this.about.Size = new System.Drawing.Size(40, 14);
            this.about.TabIndex = 11;
            this.about.Text = "About";
            this.about.Click += new System.EventHandler(this.about_Click);
            // 
            // StatusProgress
            // 
            this.StatusProgress.Location = new System.Drawing.Point(10, 81);
            this.StatusProgress.Name = "StatusProgress";
            this.StatusProgress.Size = new System.Drawing.Size(427, 10);
            this.StatusProgress.TabIndex = 13;
            // 
            // version
            // 
            this.version.AutoSize = true;
            this.version.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.version.Location = new System.Drawing.Point(210, 116);
            this.version.Name = "version";
            this.version.Size = new System.Drawing.Size(28, 13);
            this.version.TabIndex = 15;
            this.version.Text = "v1.3";
            // 
            // progressBarEx1
            // 
            this.progressBarEx1.Location = new System.Drawing.Point(10, 81);
            this.progressBarEx1.Name = "progressBarEx1";
            this.progressBarEx1.Size = new System.Drawing.Size(427, 10);
            this.progressBarEx1.TabIndex = 14;
            // 
            // PatcherWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 149);
            this.Controls.Add(this.version);
            this.Controls.Add(this.progressBarEx1);
            this.Controls.Add(this.StatusProgress);
            this.Controls.Add(this.about);
            this.Controls.Add(this.Invoxi);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.RestoreOriginalButton);
            this.Controls.Add(this.IPAURLBox);
            this.Controls.Add(this.IPAURLLabel);
            this.Controls.Add(this.PatchButton);
            this.Controls.Add(this.IPASelector);
            this.Controls.Add(this.UtilityLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PatcherWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AltServer Patcher";
            this.Load += new System.EventHandler(this.PatcherWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label UtilityLabel;
        private System.Windows.Forms.ComboBox IPASelector;
        private System.Windows.Forms.Button PatchButton;
        private System.Windows.Forms.Label IPAURLLabel;
        private System.Windows.Forms.TextBox IPAURLBox;
        private System.Windows.Forms.LinkLabel RestoreOriginalButton;
        private System.Windows.Forms.Label Invoxi;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Label about;
        private System.Windows.Forms.ProgressBar StatusProgress;
        private QuantumConcepts.Common.Forms.UI.Controls.ProgressBarEx progressBarEx1;
        private System.Windows.Forms.Label version;
    }
}

