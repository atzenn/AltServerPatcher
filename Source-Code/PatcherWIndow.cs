﻿using AutoUpdaterDotNET;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace AltServerPatcher
{

    public partial class PatcherWindow : Form
    {
        private static string EXEPath = "";
        private static bool IsAdministrator => new WindowsPrincipal(WindowsIdentity.GetCurrent()).IsInRole(WindowsBuiltInRole.Administrator);

        private static byte[] EXEBytes;

        private static int URLStart = 0;

        private static string CURRENT_URL = "";


        public PatcherWindow()
        {
            InitializeComponent();
        }


        private byte[] CreateSpacedString(string ToSpace)
        {
            byte[] first = Encoding.ASCII.GetBytes(ToSpace);
            List<byte> output = new List<byte>();
            for (int i = 0; i < first.Length; i++)
            {
                output.Add(first[i]);
                output.Add(0);
            }

            return output.ToArray();
        }

        private string CreateNormalString(string ToNormal)
        {
            byte[] first = Encoding.ASCII.GetBytes(ToNormal);
            List<byte> output = new List<byte>();
            for (int i = 0; i < first.Length; i+=2)
            {
                output.Add(first[i]);
            }

            return Encoding.ASCII.GetString(output.ToArray());
        }

        private string PadTo55(string ToPad)
        {
            if (ToPad.Length == 55)
            {
                return ToPad;
            }
            else if (ToPad.Length < 55)
            {
                string output;
                if (ToPad.Contains("?"))
                {
                    output = ToPad + "&";
                }
                else
                {
                    output = ToPad + "?";
                }
                while (output.Length < 55)
                {
                    output += "a";
                }
                return output;
            }
            else
            {
                return "https://ipg.pw/altstore/?ipa=unc0ver";
            }
        }

        private void PatcherWindow_Load(object sender, EventArgs e)
        {
            progressBarEx1.Visible = false;
            StatusProgress.Visible = true;

            IPASelector.SelectedIndex = 0;

            if (!IsAdministrator)
            {
                MessageBox.Show("Please run AltServer Patcher as an administrator account.", "Insufficient Priviliges", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
                Application.Exit();
                return;
            }
            AutoUpdater.Start("https://raw.githubusercontent.com/laithayoub71/SDIEgui/master/altserver.xml");
            AutoUpdater.Mandatory = true;

            if (File.Exists("C:\\Program Files (x86)\\AltServer\\AltServer.exe"))
            {
                EXEPath = "C:\\Program Files (x86)\\AltServer\\AltServer.exe";
            } 
            else if (File.Exists("C:\\Program Files\\AltServer\\AltServer.exe"))
            {
                EXEPath = "C:\\Program Files\\AltServer\\AltServer.exe";
            }
            if (File.Exists("AltServerPatcher.ini"))
            {
                var readPath = File.ReadAllText("AltServerPatcher.ini");
                var replaceSlash = readPath.Replace(@"\", @"\\");
                File.WriteAllText("AltServerPatcher.ini", replaceSlash);
                EXEPath = readPath;
            }
            else
            {
                MessageBox.Show(
                "Please locate your AltServer.exe file",
                "Can't Locate AltServer",
                MessageBoxButtons.OK,
                MessageBoxIcon.Exclamation);

                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.FileName = "AltServer.exe";
                openFileDialog1.Filter = "AltServer.exe|*.exe";
                openFileDialog1.FilterIndex = 1;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string finalFile = openFileDialog1.FileName;
                    EXEPath = finalFile;
                    var altServerINI = @"AltServerPatcher.ini";
                    var altServerPath = Environment.ExpandEnvironmentVariables(altServerINI);
                    TextWriter streamAltServer = new StreamWriter(altServerPath);
                    streamAltServer.WriteLine(EXEPath);
                    streamAltServer.Close();
                    var readPath = File.ReadAllText("AltServerPatcher.ini");
                    var removeSpaces = readPath.Trim();
                    File.WriteAllText("AltServerPatcher.ini", removeSpaces);
                }
            }

            if (!File.Exists(EXEPath + ".bak"))
            {
                File.Copy(EXEPath, EXEPath + ".bak");
            }

            EXEBytes = File.ReadAllBytes(EXEPath);
            byte[] HTTPBytes = CreateSpacedString("http");
            URLStart = EXEBytes.Locate(HTTPBytes)[0];

            List<byte> foundURL = new List<byte>();
            for (int i = URLStart; i < URLStart+110; i ++)
            {
                foundURL.Add(EXEBytes[i]);
            }
            CURRENT_URL = CreateNormalString(Encoding.ASCII.GetString(foundURL.ToArray()));
            
            if (CURRENT_URL == "https://ipg.pw/altstore/?ipa=unc0ver")
            {
                IPASelector.SelectedIndex = 0;
            } else if (CURRENT_URL.StartsWith("https://ipg.pw/altstore/?ipa="))
            {
                string prepad = CURRENT_URL.Split('&')[0];
                string name = prepad.Replace("https://ipg.pw/altstore/?ipa=", "");
                IPASelector.SelectedItem = name;
            } else
            {
                IPASelector.SelectedIndex = IPASelector.Items.Count - 1;
                IPAURLBox.Text = CURRENT_URL;
            }
        }

        private void IPASelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IPASelector.SelectedIndex == 3)
            {
                IPAURLLabel.Show();
                IPAURLBox.Enabled = true;
                IPAURLBox.Show();
                StatusProgress.Location = new System.Drawing.Point(15, 81);
                progressBarEx1.Location = new System.Drawing.Point(15, 81);
                RestoreOriginalButton.Location = new System.Drawing.Point(12, 129);
                Invoxi.Location = new System.Drawing.Point(295, 130);
                about.Location = new System.Drawing.Point(204, 97);
                version.Location = new System.Drawing.Point(210, 116);
                this.Width = 465;
                this.Height = 188;
                IPAURLBox.ReadOnly = false;
                IPAURLBox.Text = "https://";
            } else if (IPASelector.SelectedIndex == 2)
            {
                IPAURLLabel.Hide();   
                IPAURLBox.Text = "https://";
                IPAURLBox.Hide();
                StatusProgress.Location = new System.Drawing.Point(15, 44);
                progressBarEx1.Location = new System.Drawing.Point(15, 44);
                RestoreOriginalButton.Location = new System.Drawing.Point(12, 92);
                Invoxi.Location = new System.Drawing.Point(295, 91);
                about.Location = new System.Drawing.Point(204, 60);
                version.Location = new System.Drawing.Point(210, 79);
                this.Width = 465;
                this.Height = 150;
            } else
            {
                IPAURLLabel.Hide();
                IPAURLBox.Text = "https://ipg.pw/altstore/?ipa=" + IPASelector.SelectedItem;
                IPAURLBox.Hide();
                StatusProgress.Location = new System.Drawing.Point(15, 44);
                progressBarEx1.Location = new System.Drawing.Point(15, 44);
                RestoreOriginalButton.Location = new System.Drawing.Point(12, 92);
                Invoxi.Location = new System.Drawing.Point(295, 91);
                about.Location = new System.Drawing.Point(204, 60);
                version.Location = new System.Drawing.Point(210, 79);
                this.Width = 465;
                this.Height = 150;
            }
        }

        private void PatchButton_Click(object sender, EventArgs e)
        {
            progressBarEx1.Visible = false;
            StatusProgress.Visible = true;
            Thread patchThread = new Thread(GoPatch);
            patchThread.Start();
        }

        // A huge thanks to MuStAPro (u/mustapro) for helping me and for providing this code below!
        private static string ShorternURL(string _url)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create("https://bit.do/mod_perl/url-shortener.pl");
            webRequest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            webRequest.Method = "POST";
            webRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0";
            webRequest.Accept = "*/*";

            string postData = "action=shorten&url=" + _url.Replace("%25", "%2525") + "&url2=+site2+&url_hash=&url_stats_is_private=0&permasession=1581363891%7Co0wqcy3od6";
            var data = Encoding.ASCII.GetBytes(postData);

            webRequest.ContentLength = data.Length;

            using (var stream = webRequest.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)webRequest.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            string shortUrl = string.Format("http://bit.do/{0}", new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(responseString)["url_hash"].ToString());
            return shortUrl;
        }

        private void GoPatch()
        {
            SetProgress(ProgressBarStyle.Marquee, 0, 100, 0);
            SetStatus("Quitting AltServer...");
            Process.Start("taskkill", "/f /im AltServer.exe");
            Thread.Sleep(1000);

            SetStatus("Patching...");
            SetEnabled(false);

            string url = IPAURLBox.Text;
            if (!url.StartsWith("https://") && !url.StartsWith("http://"))
            {
                SetProgress(ProgressBarStyle.Continuous, 0, 100, 0);
                SetStatus("Please enter a valid URL.");
                SetEnabled(true);
                return;
            }
            if (url.Length > 55)
            {
                MessageBox.Show(
                "Since your link is over 55 characters it can be shortened. This feature is experimental and sometimes you might get an 'Error 503' form the server.\r\n\r\nIf that happens, just restart this program and try again.",
                "Experimental URL Shortener",
                MessageBoxButtons.OK,
                MessageBoxIcon.Exclamation);
                SetProgress(ProgressBarStyle.Continuous, 0, 100, 0);
                if (url.Contains("bit.do"))
                {
                    SetStatus("URL already shortened, proceeding to the next step...");
                }
                else
                {
                    SetStatus("Shortening the URL...");
                    url = ShorternURL(url);
                }
            }

            string paddedURL = url;

            if ((!paddedURL.Contains("?a")) || (paddedURL.Length < 55))
            {
                SetStatus("Padding out the URL...");

                paddedURL = PadTo55(url);
            }

            SetStatus("Creating the URL byte array...");
            byte[] byteURL = CreateSpacedString(paddedURL);

            SetStatus("Replacing the bytes...");
            SetProgress(ProgressBarStyle.Continuous, 0, 110, 0);
            int x = 0;
            for (int i = URLStart; i < URLStart + 110; i++)
            {
                SetProgress(ProgressBarStyle.Continuous, x, 110, 0);
                EXEBytes[i] = byteURL[x];
                x++;
            }

            SetStatus("Writing to the file...");
            SetProgress(ProgressBarStyle.Marquee, 0, 100, 0);
            File.WriteAllBytes(EXEPath, EXEBytes);

            SetStatus("Done!");
            SetProgress(ProgressBarStyle.Continuous, 100, 100, 0);
            SetEnabled(true);
            MessageBox.Show("AltServer has been patched to install your IPA file! Make sure you uninstall any other applications installed with AltServer to prevent issues.", "AltServer Patcher", MessageBoxButtons.OK, MessageBoxIcon.Information);
            
            
            SetProgress(ProgressBarStyle.Marquee, 0, 100, 0);
            SetStatus("Restarting AltServer...");
            Process.Start(EXEPath);
            Thread.Sleep(1000);


            SetProgress(ProgressBarStyle.Continuous, 100, 100, 0);
            SetStatus("AltServer restarted!");
            SetEnabled(true);
        }

        delegate void SetStatusCallback(string text);
        private void SetStatus(string text)
        {
            if (this.StatusLabel.InvokeRequired)
            {
                SetStatusCallback d = new SetStatusCallback(SetStatus);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.StatusLabel.Text = text;
            }
        }

        delegate void SetEnabledCallback(bool enabled);
        private void SetEnabled(bool enabled)
        {
            if (this.IPAURLBox.InvokeRequired)
            {
                SetEnabledCallback d = new SetEnabledCallback(SetEnabled);
                this.Invoke(d, new object[] { enabled });
            }
            else
            {
                IPAURLBox.Enabled = enabled;
                IPASelector.Enabled = enabled;
                PatchButton.Enabled = enabled;
            }
        }

        delegate void SetProgressCallback(ProgressBarStyle progress, int count, int max, int min);
        private void SetProgress(ProgressBarStyle progress, int count, int max, int min)
        {
            if (this.StatusProgress.InvokeRequired)
            {
                SetProgressCallback d = new SetProgressCallback(SetProgress);
                Invoke(d, new object[] { progress, count, max, min });
            }
            else
            {
                StatusProgress.Style = progress;
                StatusProgress.Minimum = min;
                StatusProgress.Maximum = max;
                StatusProgress.Value = count;
            }
        }

        private void RestoreOriginalButton_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SetProgress(ProgressBarStyle.Marquee, 0, 100, 0);
            SetStatus("Quitting AltServer...");
            Process.Start("taskkill", "/f /im AltServer.exe");
            Thread.Sleep(1000);
            File.Copy(EXEPath + ".bak", EXEPath, true);
            if (File.Exists(EXEPath + ".bak") && (File.Exists(EXEPath)))
            {
                progressBarEx1.Visible = true;
                StatusProgress.Visible = false;
                SetStatus("Successfully restored!");
                progressBarEx1.ForeColor = Color.FromArgb(0, 0, 255);
            }
            else
            {
                progressBarEx1.Visible = true;
                StatusProgress.Visible = false;
                SetStatus("Restore failed! Backup file may be corrupt/missing. Please try again.");
                progressBarEx1.ForeColor = Color.FromArgb(255, 0, 0);
            }
        }

        private void Invoxi_Click(object sender, EventArgs e)
        {
            Process.Start("https://twitter.com/InvoxiPlayGames");
        }

        private void about_Click(object sender, EventArgs e)
        {
          MessageBoxManager.OK = "Nice! :D";
          MessageBoxManager.Register();
          MessageBox.Show(
          "What is This Program?\r\n\r\nAltServer Patcher tricks AltServer into letting it install and sign any IPA file to your iOS device.\r\n\r\n------------------------------------------------------------------------------\r\n\r\nHow to Use:\r\n\r\n1. Choose one of the preconfigured options or enter a link to your custom IPA file. Only direct URLs are supported.\r\n(URL must end with '.ipa')\r\n\r\n2. Click 'Patch' and wait until process is done.\r\n\r\n3. Open AltServer from your taskbar and click 'Install AltStore' and click on your device. Then follow on screen instructions.\r\n\r\nNOTE: This won't actually install AltStore, it will install the IPA file you chose.\r\n\r\n------------------------------------------------------------------------------\r\n\r\nThis program was created by InvoxiPlayGames and was updated and modified by MuStAPro & FutureFlash on 2/11/2020.\r\n\r\nIf you paid for this program, request a refund because it is free and always will be, thanks to Invoxi <3\r\n\r\nHappy sideloading,\r\nFutureFlash",
          "AltServer Patcher (v1.3)",
          MessageBoxButtons.OK,
          MessageBoxIcon.Information);
          MessageBoxManager.Unregister();
        }
    }
}
