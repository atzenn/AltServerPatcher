﻿# AltServerPatcher

## In this Fork:
* Improved overall UI
* Simplified program and error messages
* Improved AltStore detection
* Added a feature where AltServer automatically restarts when patching is done
* Added an "About" button that shows information about the program and how to use it

--------------------------------------------------------------------------------
Reddit Post: https://www.reddit.com/r/jailbreak/comments/f1j55e/update_altserver_patcher_patch_altserver_to/

--------------------------------------------------------------------------------

* AltServer was created by InvoxiPlayGames and was updated by FutureFlash (me :D)

## Credits

InvoxiPlayGames: https://twitter.com/InvoxiPlayGames

FutureFlash: https://www.youtube.com/c/TypicalTechTuber